# Skins collectible

Wear and unlock skins on Minetest  
  
<a href="https://liberapay.com/Zughy/"><img src="https://i.imgur.com/4B2PxjP.png" alt="Support my work"/></a>  

### Want to help?
Feel free to:
* open an [issue](https://gitlab.com/zughy-friends-minetest/skins-collectible/-/issues)
* submit a merge request. In this case, PLEASE, do follow milestones and my [coding guidelines](https://cryptpad.fr/pad/#/2/pad/view/-l75iHl3x54py20u2Y5OSAX4iruQBdeQXcO7PGTtGew/embed/). I won't merge features for milestones that are different from the upcoming one (if it's declared), nor messy code
* join the dev room of my Minetest server (A.E.S.) on [Matrix](https://matrix.to/#/%23minetest-aes-dev:matrix.org)

### Credits
* Deny sound by [suntemple](https://freesound.org/people/suntemple/sounds/249300/) (tweaked by me)

Images are under [CC BY-SA 4.0](https://creativecommons.org/licenses/by-sa/4.0/)
