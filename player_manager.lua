minetest.register_on_joinplayer(function(player)
  local p_name = player:get_player_name()
  minetest.after(0,function(p_name)
    local player = minetest.get_player_by_name(p_name)
    if player then
      skins_collectible.load_player_data(player)
    end  
  end,p_name)
  
end)
